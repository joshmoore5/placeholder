package jam.appcamp.com.mealsformev2.adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.support.v7.widget.PopupMenu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.vstechlab.easyfonts.EasyFonts;

import jam.appcamp.com.mealsformev2.R;
import jam.appcamp.com.mealsformev2.objects.Food;
import jam.appcamp.com.mealsformev2.objects.Meal;

/**
 * Created by Josh on 8/21/2015.
 */
public class FoodChoiceListAdapter extends ArrayAdapter<String>{

    Activity context;
    Meal meal;

    public FoodChoiceListAdapter(Activity context, Meal meal) {
        super(context, R.layout.food_view);
        this.context = context;
        this.meal = meal;
    }

    @Override
    public int getCount(){
        return meal.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //make sure we have a view
        View itemView = convertView;
        if (itemView == null) {
            itemView = context.getLayoutInflater().inflate(R.layout.food_view, parent, false);
        }

        Food currentFood = meal.get(position);
        ImageView imageView = (ImageView) itemView.findViewById(R.id.item_picture);

        imageView.setImageBitmap(currentFood.getIcon());

        TextView makeFood1 = (TextView) itemView.findViewById(R.id.item_foodName);
        makeFood1.setText(currentFood.getName());
        makeFood1.setTypeface(EasyFonts.robotoThin(context));

        return itemView;

    }

}
