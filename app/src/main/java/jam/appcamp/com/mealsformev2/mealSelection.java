package jam.appcamp.com.mealsformev2;

import android.app.ActivityManager;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.MediaStore;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.vstechlab.easyfonts.EasyFonts;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Locale;

import jam.appcamp.com.mealsformev2.adapters.FoodChoiceListAdapter;
import jam.appcamp.com.mealsformev2.adapters.FoodListAdapter;
import jam.appcamp.com.mealsformev2.adapters.MealListAdapter;
import jam.appcamp.com.mealsformev2.objects.Food;
import jam.appcamp.com.mealsformev2.objects.Meal;

public class mealSelection extends ActionBarActivity implements ListView.OnItemLongClickListener {

    private ViewFlipper vf;
    private ImageButton FAB1;
    private ImageButton FAB2;
    private ListView meals;
    private ListView mealsFood;
    private ListView foods;
    private Toolbar toolbar[];
    private TextView toolbarTitle[];
    private TextView TimerView, textView, last;

    private ArrayList<Meal> myMeals;
    private Meal savedFoods;
    private Meal currentMeal;
    private Food newFood;
    private TextToSpeech t1;
    private MediaPlayer mp1;
    private MediaPlayer mp2;
    private MediaPlayer mp3;
    private MediaPlayer sand;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meal_selection);

        newFood = new Food();
        savedFoods = new Meal();

        t1 = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status != TextToSpeech.ERROR) {
                    t1.setLanguage(Locale.UK);
                    t1.setSpeechRate(1);
                }
            }
        });

        //saved foods
        Bitmap placeholder = BitmapFactory.decodeResource(getResources(), R.drawable.pizza);
        Bitmap placeholder2 = BitmapFactory.decodeResource(getResources(), R.drawable.apple2);
        Bitmap placeholder3 = BitmapFactory.decodeResource(getResources(), R.drawable.cakee);
        Bitmap placeholder4 = BitmapFactory.decodeResource(getResources(), R.drawable.burger2);
        Bitmap placeholder5 = BitmapFactory.decodeResource(getResources(), R.drawable.cheese);
        Bitmap placeholder6 = BitmapFactory.decodeResource(getResources(), R.drawable.brocolli);
        Bitmap placeholder7 = BitmapFactory.decodeResource(getResources(), R.drawable.chicken);
        Bitmap placeholder8 = BitmapFactory.decodeResource(getResources(), R.drawable.fishfingers);
        Bitmap placeholder9 = BitmapFactory.decodeResource(getResources(), R.drawable.nachos);
        Bitmap placeholder10 = BitmapFactory.decodeResource(getResources(), R.drawable.orangesmall);
        Bitmap placeholder11= BitmapFactory.decodeResource(getResources(), R.drawable.toast);
        Bitmap placeholder12 = BitmapFactory.decodeResource(getResources(), R.drawable.tomato);
        Bitmap placeholder13 = BitmapFactory.decodeResource(getResources(), R.drawable.watermelon);
        Bitmap placeholder14 = BitmapFactory.decodeResource(getResources(), R.drawable.bananasmall);



        savedFoods.addFood(new Food("Pizza", placeholder));
        savedFoods.addFood(new Food("Cake", placeholder3));
        savedFoods.addFood(new Food("Burger", placeholder4));
        savedFoods.addFood(new Food("Cheese", placeholder5));
        savedFoods.addFood(new Food("Broccoli", placeholder6));
        savedFoods.addFood(new Food("Chicken", placeholder7));
        savedFoods.addFood(new Food("Fish Fingers", placeholder8));
        savedFoods.addFood(new Food("Nachos", placeholder9));
        savedFoods.addFood(new Food("Orange", placeholder10));
        savedFoods.addFood(new Food("Toast", placeholder11));
        savedFoods.addFood(new Food("Tomato", placeholder12));
        savedFoods.addFood(new Food("Watermelon", placeholder13));
        savedFoods.addFood(new Food("Banana", placeholder14));
        savedFoods.addFood(new Food("Apple", placeholder2));


        vf = (ViewFlipper) findViewById(R.id.switcher);

        FAB1 = (ImageButton) findViewById(R.id.fabMealSelec);
        FAB2 = (ImageButton) findViewById(R.id.FAB2);

        meals = (ListView) findViewById(R.id.mealsList);
        mealsFood = (ListView) findViewById(R.id.MealsFoods);
        foods = (ListView) findViewById(R.id.food_view);

        TimerView = (TextView) findViewById(R.id.timer);
        textView = (TextView) findViewById(R.id.timerTextView);
        last = (TextView) findViewById(R.id.textView10);

        TimerView.setTypeface(EasyFonts.robotoThin(this));
        textView.setTypeface(EasyFonts.robotoThin(this));
        last.setTypeface(EasyFonts.robotoThin(this));


        myMeals = new ArrayList<>();
        toolbar = new Toolbar[5];
        toolbarTitle = new TextView[5];
        toolbar[0] = (Toolbar) findViewById(R.id.tool_bar);
        toolbar[1] = (Toolbar) findViewById(R.id.tool_bar2);
        toolbar[2] = (Toolbar) findViewById(R.id.tool_bar3);
        toolbar[3] = (Toolbar) findViewById(R.id.tool_bar4);
        toolbar[4] = (Toolbar) findViewById(R.id.tool_bar5);

        toolbarTitle[0] = (TextView) findViewById(R.id.toolbar_title);
        toolbarTitle[1] = (TextView) findViewById(R.id.toolbar_title2);
        toolbarTitle[2] = (TextView) findViewById(R.id.toolbar_title3);
        toolbarTitle[3] = (TextView) findViewById(R.id.toolbar_title4);
        toolbarTitle[4] = (TextView) findViewById(R.id.toolbar_title5);


        toolbarTitle[0].setTypeface(EasyFonts.robotoThin(this));
        toolbarTitle[1].setTypeface(EasyFonts.robotoThin(this));
        toolbarTitle[2].setTypeface(EasyFonts.robotoThin(this));
        toolbarTitle[3].setTypeface(EasyFonts.robotoThin(this));
        toolbarTitle[4].setTypeface(EasyFonts.robotoThin(this));

        toolbar[0].setTitle("");
        toolbar[1].setTitle("");
        toolbar[2].setTitle("");
        toolbar[3].setTitle("");
        toolbar[4].setTitle("");

        setSupportActionBar(toolbar[0]);

        mp1 = MediaPlayer.create(this, R.raw.boop);
        mp2 = MediaPlayer.create(this, R.raw.cameraclick);
        mp3 = MediaPlayer.create(this, R.raw.tick);
        sand = MediaPlayer.create(this, R.raw.sand);

        myMeals = readMeals();

        meals.setAdapter(new MealListAdapter(mealSelection.this, myMeals));


        FAB1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                myMeals.add(new Meal());
                currentMeal = myMeals.get(myMeals.size() - 1);
                meals.setAdapter(new MealListAdapter(mealSelection.this, myMeals));
                mp1.start();
                vf.showNext();
                writeMeals(false);
                loadMealCreator();

            }
        });

        meals.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                currentMeal = myMeals.get(position);
                mp1.start();
                vf.showNext();
                writeMeals(false);
                loadMealCreator();

            }
        });

        meals.setOnItemLongClickListener(this);
    }

    @Override
    public void onBackPressed() {
        if (vf.getDisplayedChild() == 0) {
            writeMeals(true);

        } else if(vf.getDisplayedChild() == 1 ) {
            vf.showPrevious();
            loadMealList();
            loadMealCreator();
        }
        else if(vf.getDisplayedChild() == 2 ) {
            vf.showPrevious();
            loadMealList();
            loadMealCreator();
        }
        else if(vf.getDisplayedChild() == 3 ) {

        }
        else if(vf.getDisplayedChild() == 4 ) {


        }
    }


    public void loadMealList() {

        meals.setAdapter(new MealListAdapter(mealSelection.this, myMeals));

    }

    public void loadMealCreator() {
        setSupportActionBar(toolbar[1]);
        mealsFood.setAdapter(new FoodListAdapter(mealSelection.this, currentMeal));

        FAB2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mp1.start();
                vf.showNext();
                writeMeals(false);
                setupFoodList();

            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if (vf.getDisplayedChild() == 2) {
            getMenuInflater().inflate(R.menu.menu_food_list, menu);
        } else if (vf.getDisplayedChild() == 1) {
            getMenuInflater().inflate(R.menu.menu_food_choice, menu
            );
        }
        return true;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public void setupFoodList() {

        setSupportActionBar(toolbar[2]);

        System.out.println("Setting up food");

        FoodChoiceListAdapter adapter = new FoodChoiceListAdapter(mealSelection.this, savedFoods);

        foods.setAdapter(adapter);
        foods.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                currentMeal.addFood(savedFoods.get(position));

                String toSpeak = savedFoods.get(position).getName();
                t1.speak(toSpeak, TextToSpeech.QUEUE_FLUSH, null);
                vf.showPrevious();
                writeMeals(false);
                loadMealCreator();

            }
        });

    }

    public void startMethod(MenuItem item) {

            vf.setDisplayedChild(3);
            writeMeals(false);
            setSupportActionBar(toolbar[3]);
            timerHandler(0);


    }

    public void timerHandler(int i) {

        if (i != currentMeal.size()) {

            runTimer(i);

        } else {
            sand.stop();
            vf.setDisplayedChild(4);
            writeMeals(false);
            String toSpeak = toolbarTitle[4].getText().toString();
            t1.speak(toSpeak, TextToSpeech.QUEUE_FLUSH, null);
            new CountDownTimer(10000, 1000) {

                public void onTick(long millisUntilFinished) {
                    textView.setText("Seconds remaining: " + millisUntilFinished / 1000);
                }

                public void onFinish() {
                    vf.setDisplayedChild(1);
                    writeMeals(false);
                }
            }.start();


        }


    }

    public void runTimer(final int index) {
        final ImageView img2 = (ImageView) findViewById(R.id.imageView3);
        ImageView img3 = (ImageView) findViewById(R.id.imageView4);
        ImageView foodIcon = (ImageView) findViewById(R.id.foodIcon);
        final TextView tct = (TextView) findViewById(R.id.timer);
        TextView name = (TextView) findViewById(R.id.toolbar_title4);

        Drawable myDrawable = getResources().getDrawable(R.drawable.bigtimerbackground);
        img3.setImageDrawable(myDrawable);

        final Integer Hourglass[] = {
                R.drawable.hourglass_frame1_3x, R.drawable.hourglass_frame1_3x, R.drawable.hourglass_frame2_3x, R.drawable.hourglass_frame2_3x, R.drawable.hourglass_frame3_x3, R.drawable.hourglass_frame3_x3,
                R.drawable.hourglass_frame4_3x, R.drawable.hourglass_frame4_3x, R.drawable.hourglass_frame5_3x, R.drawable.hourglass_frame5_3x,
                R.drawable.hourglass_frame6_3x, R.drawable.hourglass_frame6_3x, R.drawable.hourglass_frame7_3x, R.drawable.hourglass_frame7_3x, R.drawable.hourglass_frame8_3x, R.drawable.hourglass_frame8_3x,
                R.drawable.hourglass_frame9_3x, R.drawable.hourglass_frame9_3x, R.drawable.hourglass_frame10_3x, R.drawable.hourglass_frame10_3x,
                //20
                R.drawable.hourglass_frame11_3x, R.drawable.hourglass_frame11_3x, R.drawable.hourglass_frame12_3x, R.drawable.hourglass_frame12_3x, R.drawable.hourglass_frame13_3x, R.drawable.hourglass_frame13_3x,
                R.drawable.hourglass_frame14_3x, R.drawable.hourglass_frame14_3x, R.drawable.hourglass_frame15_3x, R.drawable.hourglass_frame15_3x, R.drawable.hourglass_frame16_3x, R.drawable.hourglass_frame16_3x,
                R.drawable.hourglass_frame17_3x, R.drawable.hourglass_frame17_3x, R.drawable.hourglass_frame18_3x, R.drawable.hourglass_frame18_3x, R.drawable.hourglass_frame19_3x, R.drawable.hourglass_frame19_3x,
                R.drawable.hourglass_frame20_3x, R.drawable.hourglass_frame20_3x,
                //20


                R.drawable.hourglass_frame21_3x, R.drawable.hourglass_frame22_3x, R.drawable.hourglass_frame23_3x,
                R.drawable.hourglass_frame24_3x, R.drawable.hourglass_frame25_3x, R.drawable.hourglass_frame26_3x,
                R.drawable.hourglass_frame27_3x, R.drawable.hourglass_frame28_3x, R.drawable.hourglass_frame29_3x,
                R.drawable.hourglass_frame30_3x,
                //10


                R.drawable.hourglass_frame31_3x, R.drawable.hourglass_frame32_3x, R.drawable.hourglass_frame33_3x, R.drawable.hourglass_frame34_3x, R.drawable.hourglass_frame35_3x,
                R.drawable.hourglass_frame36_3x, R.drawable.hourglass_frame37_3x, R.drawable.hourglass_frame39_3x, R.drawable.hourglass_frame38_3x, R.drawable.hourglass_frame40_3x,
        };


        Food temp = currentMeal.get(index);
        final int seconds = temp.getMethNum() * 60;

        foodIcon.setImageBitmap(temp.getIcon());

        name.setText("You're eating " + temp.getName() + "!");
        String toSpeak = name.getText().toString();
        t1.speak(toSpeak, TextToSpeech.QUEUE_FLUSH, null);


        new CountDownTimer(seconds * 1000, 1000) {

            int currentImg = 0;
            boolean ticksound = true;


            public void onTick(long value) {
                int secRem = (int) value / 1000;
                tct.setText("" + secRem);

                sand.start();
                sand.setLooping(true);
                sand.setVolume(0.01f, 0.01f);


                //every change value, increment through the array list + 1.

                int change = seconds / 60;

                if (secRem % change == 0) {
                    currentImg++;
                    img2.setImageResource(Hourglass[currentImg]);
                }
                if (ticksound == true && secRem == 5) {
                    mp3.start();
                }

            }


            public void onFinish() {
                int temp = index + 1;
                timerHandler(temp);
            }
        }.start();


    }

    public void newFood(MenuItem item) {

        addNewFood();

    }

    public void addNewFood() {
        final Dialog mDialog = new Dialog(this);
        mDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        mDialog.setContentView(R.layout.new_food);

        TextView toolbar = (TextView) mDialog.findViewById(R.id.toolbar_title);
        final EditText foodName = (EditText) mDialog.findViewById(R.id.edtFoodName);
        ImageButton addImg = (ImageButton) mDialog.findViewById(R.id.imgBtnAdd);
        Button create = (Button) mDialog.findViewById(R.id.btnCreate);
        if (!newFood.isAddingImage()) {
            newFood = new Food();
        }

        if (newFood.getName() != null) {
            foodName.setText(newFood.getName());
        }

        if (newFood.getIcon() != null) {
            addImg.setImageBitmap(newFood.getIcon());
        }

        addImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                newFood.setAddingImage(true);
                newFood.setName(foodName.getText().toString());
                mp2.start();
                Intent cam = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cam, 1);
                mDialog.dismiss();

            }
        });

        create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String name = foodName.getText().toString();

                if (name.equals("")) {
                    Toast.makeText(mealSelection.this, "Please enter a name", Toast.LENGTH_SHORT);
                } else {
                    newFood.setName(name);
                    savedFoods.addFood(newFood);
                    foods.setAdapter(new FoodChoiceListAdapter(mealSelection.this, savedFoods));
                    newFood = new Food();
                    mDialog.dismiss();

                }


            }
        });

        mDialog.show();
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {

        final Dialog mDialog = new Dialog(mealSelection.this);
        mDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        mDialog.setContentView(R.layout.meal_options);

        final Meal temp = myMeals.get(position);
        ImageView icon = (ImageView) mDialog.findViewById(R.id.mealIcon);
        final EditText name = (EditText) mDialog.findViewById(R.id.edtMealName);
        Button save = (Button) mDialog.findViewById(R.id.save);
        Button delete = (Button) mDialog.findViewById(R.id.delete);
        TextView title = (TextView) mDialog.findViewById(R.id.toolbar_title);
        Toolbar toolbar = (Toolbar) mDialog.findViewById(R.id.tool_bar);

        toolbar.setTitle("");
        save.getResources().getColor(R.color.ColorPrimaryLight);
        delete.getResources().getColor(R.color.ColorPrimaryLight);


        name.setTypeface(EasyFonts.robotoThin(this));
        save.setTypeface(EasyFonts.robotoThin(this));
        delete.setTypeface(EasyFonts.robotoThin(this));
        name.setTypeface(EasyFonts.robotoThin(this));
        title.setTypeface(EasyFonts.robotoThin(this));

        //icon.setImageResource(temp.getIconID());
        icon.setImageResource(R.drawable.bowl);
        name.setText(temp.getName());

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                temp.setName(name.getText().toString());
                meals.setAdapter(new MealListAdapter(mealSelection.this, myMeals));
                mDialog.dismiss();

            }
        });

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                myMeals.remove(position);
                meals.setAdapter(new MealListAdapter(mealSelection.this, myMeals));
                mDialog.dismiss();

            }
        });
        mDialog.show();
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            try {
                Bundle extras = data.getExtras();
                Bitmap imageBitmap = (Bitmap) extras.get("data");
                newFood.setIcon(Bitmap.createScaledBitmap(imageBitmap, imageBitmap.getWidth() * 2, imageBitmap.getHeight() * 2, false));
                newFood.setAddingImage(true);
                addNewFood();

            } catch (Exception ex) {
                ex.printStackTrace();
                Toast.makeText(mealSelection.this, "IT DONE G\n" +
                        "\n" +
                        "        mDialog.show();\n" +
                        "        return true;OOFED", Toast.LENGTH_SHORT).show();
            }
        }

    }

    public void writeMeals(final boolean bool) {

        System.out.println("Test");

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {

                String filename = "meals.txt";

                File file = new File(getFilesDir(), filename);

                if (!file.isDirectory()) {

                }


                try {

                    if (!file.exists()) {
                        file.createNewFile();
                    }

                    FileOutputStream FOS = new FileOutputStream(file);
                    ObjectOutputStream oos = new ObjectOutputStream(FOS);

                    oos.writeObject(myMeals);


                    FOS.close();
                    oos.close();

                    if (bool) {
                        finish();
                    }


                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }
        });

        thread.start();
    }

    public ArrayList<Meal> readMeals() {

        ArrayList<Meal> temp = new ArrayList<>();

        try {

            File file = new File(this.getFilesDir(), "meals.txt");

            if (!file.exists()) {
                file.createNewFile();
            }

            FileInputStream FIS = new FileInputStream(file);
            ObjectInputStream OIS = new ObjectInputStream(FIS);

            temp = (ArrayList<Meal>) OIS.readObject();
            FIS.close();
            OIS.close();


        } catch (Exception ex) {

            ex.printStackTrace();
        }

        return temp;
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();

        ActivityManager activityManager = (ActivityManager) getApplicationContext()
                .getSystemService(Context.ACTIVITY_SERVICE);

        activityManager.moveTaskToFront(getTaskId(), 0);
    }
}
