package jam.appcamp.com.mealsformev2.objects;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;

import java.io.Serializable;

import jam.appcamp.com.mealsformev2.R;

/**
 * Created by Josh on 8/21/2015.
 */
public class Food implements Serializable {

    private String name;
    private SerialBitmap icon;
    private String methName;
    private int methNum;
    private static final long serialVersionUID = 1L;

    private boolean addingImage = false;

    public Food(String name, Bitmap icon) {
        this.name = name;
        this.icon = new SerialBitmap(icon);
    }


    public Food(){

        this("", BitmapFactory.decodeResource(null, R.drawable.camera));

    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Bitmap getIcon() {
        return icon.getBitmap();
    }

    public void setIcon(Bitmap icon) {
        this.icon.setBitmap(icon);
    }

    public String getMethName() {
        return methName;
    }

    public void setMethName(String methName) {
        this.methName = methName;
    }

    public int getMethNum() {
        return methNum;
    }

    public void setMethNum(int methNum) {
        this.methNum = methNum;
    }

    public boolean isAddingImage() {
        return addingImage;
    }

    public void setAddingImage(boolean addingImage) {
        this.addingImage = addingImage;
    }

}
