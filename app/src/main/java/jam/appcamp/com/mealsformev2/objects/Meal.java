package jam.appcamp.com.mealsformev2.objects;

import java.io.Serializable;
import java.util.ArrayList;

import jam.appcamp.com.mealsformev2.R;

/**
 * Created by Josh on 8/21/2015.
 */
public class Meal implements Serializable{

    private String name;
    private int iconID;
    private ArrayList<Food> foods;
    private static final long serialVersionUID = 1L;


    public Meal(String name, int iconID) {
        this.name = name;
        this.iconID = iconID;
        foods = new ArrayList<>();
    }

    public Meal(){
        this("Meal", R.drawable.pizza);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIconID() {
        return iconID;
    }

    public void setIconID(int iconID) {
        this.iconID = iconID;
    }

    public ArrayList<Food> getFoods() {
        return foods;
    }

    public void setFoods(ArrayList<Food> foods) {
        this.foods = foods;
    }

    public int size(){
        return foods.size();
    }

    public void addFood(Food food){
        foods.add(food);
    }

    public Food get(int i){
        return foods.get(i);
    }

    @Override
    public String toString(){
        String temp = "";
        temp += String.format("Name: %s\n", name);

        for (Food cur : foods) {
            temp += cur.getName();
        }

        return temp;
    }

    public Meal copy() {
        Meal copy = new Meal(this.getName(), this.getIconID());
        copy.setFoods(this.getFoods());
        return copy;
    }


}
