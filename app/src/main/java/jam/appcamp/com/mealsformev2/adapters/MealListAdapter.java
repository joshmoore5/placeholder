package jam.appcamp.com.mealsformev2.adapters;

import android.app.Activity;
import android.graphics.Bitmap;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.vstechlab.easyfonts.EasyFonts;

import java.util.ArrayList;

import jam.appcamp.com.mealsformev2.R;
import jam.appcamp.com.mealsformev2.objects.Meal;

/**
 * Created by Josh on 8/22/2015.
 */
public class MealListAdapter extends ArrayAdapter<String> {

    private Activity context;
    ArrayList<Meal> meals;

    public MealListAdapter(Activity context, ArrayList<Meal> meals){
        super(context, R.layout.item_view);
        this.context = context;
        this.meals = meals;
    }

    @Override
    public int getCount(){
        return meals.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //make sure we have a view
        View itemView = convertView;
        if(itemView == null){
            itemView = context.getLayoutInflater().inflate(R.layout.item_view,parent,false);
        }

        Meal currentMeal = meals.get(position);

        ImageView imageView = (ImageView) itemView.findViewById(R.id.item_Icon);

        if (!(currentMeal.size() == 0)){

            Bitmap first = currentMeal.get(0).getIcon();

            imageView.setImageBitmap(first);

        }else{

            imageView.setImageResource(currentMeal.getIconID());

        }


        TextView makeTitle = (TextView) itemView.findViewById(R.id.item_mealName);
        makeTitle.setText(currentMeal.getName());
        makeTitle.setTypeface(EasyFonts.robotoThin(context));



        return itemView;

        //  return super.getView(position, convertView, parent);
    }

}
