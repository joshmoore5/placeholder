package jam.appcamp.com.mealsformev2.adapters;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import com.vstechlab.easyfonts.EasyFonts;

import java.util.Random;

import jam.appcamp.com.mealsformev2.R;
import jam.appcamp.com.mealsformev2.objects.Food;
import jam.appcamp.com.mealsformev2.objects.Meal;

/**
 * Created by Josh on 8/21/2015.
 */
public class FoodListAdapter extends ArrayAdapter<String> implements View.OnClickListener {

    Activity context;
    Meal meal;

    Button confirm;
    TextView text;
    int minute;
    Toolbar toolbar;
    TextView textview;

    public FoodListAdapter(Activity context, Meal meal) {

        super(context, R.layout.cloud_layout);
        this.context = context;
        this.meal = meal;

    }

    @Override
    public int getCount() {
        return meal.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.cloud_layout, null, true);

        TextView Name = (TextView) rowView.findViewById(R.id.food_name);
        TextView minutes = (TextView) rowView.findViewById(R.id.minutes);

        ImageView Photo = (ImageView) rowView.findViewById(R.id.photo);

        Name.setTypeface(EasyFonts.robotoThin(context));
        minutes.setTypeface(EasyFonts.robotoThin(context));



        TextView method = (TextView) rowView.findViewById(R.id.method);
        method.setTypeface(EasyFonts.robotoThin(context));
        ImageButton button = (ImageButton) rowView.findViewById(R.id.imageButton2);
        button.setId(new Integer(position));



        Food food = (meal.get(position));
        Name.setText(food.getName());
        Photo.setImageBitmap(food.getIcon());
        method.setText(food.getMethName());
        minutes.setText("" + food.getMethNum() + " minutes selected.");

        button.setOnClickListener(this);

//        rowView.setId(new Integer(position));
        return rowView;
    }

    @Override
    public void onClick(final View v) {
        final Dialog mDialog = new Dialog(context);
        mDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        mDialog.setContentView(R.layout.timer);
        toolbar = (Toolbar) mDialog.findViewById(R.id.tool_bar);
        confirm = (Button) mDialog.findViewById(R.id.button2);
        textview = (TextView) mDialog.findViewById(R.id.toolbar_title);
        text = (TextView) mDialog.findViewById(R.id.textView9);
        toolbar.setTitle("");


        NumberPicker np = (NumberPicker) mDialog.findViewById(R.id.numberPicker2);
        np.setMaxValue(10);
        np.setMinValue(1);
        np.setWrapSelectorWheel(true);

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mDialog.dismiss();
                refresh();


            }
        });
        np.setOnValueChangedListener(new NumberPicker.
                OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int
                    oldVal, int newVal) {
                text.setText("" +
                        newVal + "  Minutes");
                minute = newVal;

                meal.get(v.getId()).setMethNum(newVal); //sets the food method to timer and the minute.

            }
        });


        mDialog.show();
    }

    public void refresh() {
        super.notifyDataSetInvalidated();

    }
}
