package jam.appcamp.com.mealsformev2;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends Activity {
    private boolean lala = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final MediaPlayer mp = MediaPlayer.create(this, R.raw.music);


        new CountDownTimer(2500, 1000) {

            public void onTick(long millisUntilFinished) {
                if(lala == true) {
                    mp.start();
                    lala = false;
                }

            }

            public void onFinish() {
                Intent i = new Intent(MainActivity.this,mealSelection.class);
                startActivity(i);
                finish();
            }
        }.start();



    }


}
